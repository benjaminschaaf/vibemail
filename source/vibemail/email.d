module vibemail.email;

import std.utf;
import std.conv;
import std.uuid;
import std.ascii;
import std.range;
import std.array;
import std.base64;
import std.string;
import std.traits;
import std.algorithm;

import vibe.d;
import vibe.mail.smtp;
import vibe.inet.message;

@safe:

struct QuotedPrintable(Range) {
	static assert(is(Unqual!(ElementType!Range) == char),
		"QuotedPrintable can only be used with a char range. Use byChar for other encodings.");

	private {
		enum WhitespaceEncoding { unknown, plain, encoded }

		Range m_range;
		char m_charOut;
		char[2] m_buffer = "\0\0";
		ubyte m_charLineCount;
		bool m_popNext = true;
		WhitespaceEncoding m_whitespaceEncoding;
	}

	this(Range range)
	@nogc {
		m_range = range;

		evaluateNext();
	}

	private void emplaceCharAsHex(in char c)
	@nogc {
		auto msb = c >> 4;
		if (msb > '\t') msb += 7;

		auto lsb = c & 0x0F;
		if (lsb > '\t') lsb += 7;

		m_charOut = '=';
		m_buffer[0] = cast(char)(48 + msb);
		m_buffer[1] = cast(char)(48 + lsb);
	}

	unittest {
		auto i = (new char[5]).byChar;
		auto range = QuotedPrintable!(typeof(i))(i);

		range.emplaceCharAsHex('\t');
		assert(range.m_charOut == '=');
		assert(range.m_buffer == "09");

		range.emplaceCharAsHex('\r');
		assert(range.m_charOut == '=');
		assert(range.m_buffer == "0D");

		range.emplaceCharAsHex('=');
		assert(range.m_charOut == '=');
		assert(range.m_buffer == "3D");

		range.emplaceCharAsHex(216);
		assert(range.m_charOut == '=');
		assert(range.m_buffer == "D8");

		range.emplaceCharAsHex(255);
		assert(range.m_charOut == '=');
		assert(range.m_buffer == "FF");
	}

	private void outputLineBreak()
	@nogc {
		m_charOut = '=';
		m_buffer = "\r\n";
		m_popNext = false;
		m_charLineCount = 0;
	}

	private void determineWhitespaceEncoding()
	@nogc {
		// we have to look ahead, if we encounter: either more characters
		// than allowed per line or when we encounter any other character
		// before 76 into the line, we can simply emit spaces and tabs
		// else we have to emit them encoded
		auto rc = m_range.save();
		auto fwdLineCount = m_charLineCount;

		m_whitespaceEncoding = WhitespaceEncoding.encoded;

		char prevChr = rc.front;
		foreach (fwdChr; rc) {
			// If the line ends before 73 we need to encode the whitespace
			if (fwdChr == '\n' && fwdLineCount <= 73) {
				return;
			}

			// We can't check the current character for a carriage return, as it may be followed by a linefeed causing a line break
			if (!isWhite(fwdChr) || prevChr == '\r' || fwdLineCount > 73) {
				m_whitespaceEncoding = WhitespaceEncoding.plain;
				return;
			}

			prevChr = fwdChr;
			fwdLineCount += 1;
		}

		// Need to handle the previous character being a carriage return, see above
		if (prevChr == '\r') {
			m_whitespaceEncoding = WhitespaceEncoding.plain;
		}
	}

	private void evaluateNext()
	@nogc {
		if (m_range.empty) {
			m_charOut = '\0';
			return;
		}

		const char chr = m_range.front;
		const iswhite = chr == ' ' || chr == '\t';

		m_popNext = true;

		if (iswhite && m_whitespaceEncoding == WhitespaceEncoding.unknown) {
			determineWhitespaceEncoding();
		}

		if (chr == '\n') {
			m_charOut = '\r';
			m_buffer = "\n\0";
			m_charLineCount = 0;
			m_whitespaceEncoding = WhitespaceEncoding.unknown;
		} else if (chr == '\r') {
			// when we encounter a carriage return, we need to lookahead to see
			// if a line-feed comes next, if it does we have a linebreak, else
			// we just encode the carriage return
			auto rc = m_range.save();
			rc.popFront();
			if (!rc.empty && rc.front == '\n') {
				// Skip the linefeed
				m_range.popFront();

				m_charOut = '\r';
				m_buffer = "\n\0";
				m_charLineCount = 0;
			} else {
				if (m_charLineCount >= 73) {
					outputLineBreak();
				} else {
					emplaceCharAsHex(chr);
					m_charLineCount += 3;
				}
			}

			m_whitespaceEncoding = WhitespaceEncoding.unknown;
		} else if (m_charLineCount == 75) {
			// Break lines at 75 characters
			outputLineBreak();
			m_whitespaceEncoding = WhitespaceEncoding.unknown;
		} else if (chr == '.' && m_charLineCount == 0) {
			// Lines starting with a dot should be encoded as 2 dots
			m_charOut = '.';
			m_buffer = ".\0";
			m_charLineCount = 2;
		} else if (chr != '=' && isGraphical(chr) || iswhite && m_whitespaceEncoding == WhitespaceEncoding.plain) {
			// Regular, unencoded characters
			m_charOut = chr;
			m_charLineCount += 1;

			if (!iswhite) {
				m_whitespaceEncoding = WhitespaceEncoding.unknown;
			}
		} else if (m_charLineCount >= 73) {
			// Encoded characters take 3 characters to encode, so do line breaks earlier
			outputLineBreak();
		} else {
			emplaceCharAsHex(chr);
			m_charLineCount += 3;

			if (!iswhite) {
				m_whitespaceEncoding = WhitespaceEncoding.unknown;
			}
		}
	}

	@property bool empty()
	const @nogc {
		return m_charOut == '\0';
	}

	void popFront()
	@nogc {
		if (m_buffer[0] == '\0') {
			if (m_popNext) m_range.popFront();

			evaluateNext();
		} else {
			m_charOut = m_buffer[0];
			m_buffer[0] = m_buffer[1];
			m_buffer[1] = '\0';
		}
	}

	@property char front() const @nogc { return m_charOut; }
}

auto quotedPrintable(R)(R range)
	@nogc
	if (isAutodecodableString!R)
{
	auto r = range.byChar;

	return QuotedPrintable!(typeof(r))(r).byDchar;
}

unittest {
	// Handles empty
	assert("".quotedPrintable.text == "");

	// any ascii char between 33 and 126 (except '='), just stays the same
	assert("abcdef".quotedPrintable.text == "abcdef");
	// any other char besides space, tab or CRLF gets encoded
	assert("⌂ασ↑▄╩ä你=".quotedPrintable.text == "=E2=8C=82=CE=B1=CF=83=E2=86=91=E2=96=84=E2=95=A9=C3=A4=E4=BD=A0=3D");

	// line breaks stay the way they are
	assert("abcdef\r\nabcdef".quotedPrintable.text == "abcdef\r\nabcdef");
	// we treat single linefeeds also as a line-break
	assert("abcdef\nabcdef".quotedPrintable.text == "abcdef\r\nabcdef");
	// but not carriage return by themselves
	assert("abcdef \r".quotedPrintable.text == "abcdef =0D");

	// spaces or tabs can be left unencoded when there are either: other characters coming on the encoded line
	assert("abc def".quotedPrintable.text == "abc def");
	// or the encoded line is longer than 75 characters
	assert("abcdef                                                                      ".quotedPrintable.text
		== "abcdef                                                                     =\r\n=20");
	// else they need to be encoded
	assert("abcdef    \t\r\n  a".quotedPrintable.text == "abcdef=20=20=20=20=09\r\n  a");
	assert("abc def  ".quotedPrintable.text == "abc def=20=20");

	// an encoded char can never be split by a required line-break. In those case the encoded line needs to be shorter than 76 chars
	assert("012345678911234567892123456789312345678941234567895123456789612345你".quotedPrintable.text
		== "012345678911234567892123456789312345678941234567895123456789612345=E4=BD=A0");
	assert("0123456789112345678921234567893123456789412345678951234567896123456你".quotedPrintable.text
		== "0123456789112345678921234567893123456789412345678951234567896123456=E4=BD=\r\n=A0");
	assert("01234567891123456789212345678931234567894123456789512345678961234567你".quotedPrintable.text
		== "01234567891123456789212345678931234567894123456789512345678961234567=E4=BD=\r\n=A0");
	assert("012345678911234567892123456789312345678941234567895123456789612345678你".quotedPrintable.text
		== "012345678911234567892123456789312345678941234567895123456789612345678=E4=BD=\r\n=A0");
	assert("0123456789112345678921234567893123456789412345678951234567896123456789你".quotedPrintable.text
		== "0123456789112345678921234567893123456789412345678951234567896123456789=E4=\r\n=BD=A0");

	// Lines starting with a '.' or wrapped into starting with a '.' should be encoded as 2 '.': ".."
	assert(".\r\n.\r\n".quotedPrintable.text == "..\r\n..\r\n");
	assert("This line is exactly 76 chars long, to make sure we put the '.' on newline-.".quotedPrintable.text
		== "This line is exactly 76 chars long, to make sure we put the '.' on newline-=\r\n..");

	// line-breaks should handle wrapping and whitespace properly
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefab\r\n ".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefab\r\n=20");
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc\r\n ".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc\r\n=20");
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcd\r\n ".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc=\r\nd\r\n=20");
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcde\r\n ".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc=\r\nde\r\n=20");

	// Encoded carriage return should wrap properly
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcde\rabcdef".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcde=0Da=\r\nbcdef");
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef\rabcdef".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdef=0D=\r\nabcdef");
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefa\rabcdef".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefa=\r\n=0Dabcdef");
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefab\rabcdef".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefab=\r\n=0Dabcdef");
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc\rabcdef".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc=\r\n=0Dabcdef");
	assert("abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcd\rabcdef".quotedPrintable.text
		== "abcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabcdefabc=\r\nd=0Dabcdef");
}

struct MailPart(R) {
	static assert(isSomeChar!(ElementType!R), "MailPart is only usable with character ranges");

	R content;
	string[string] headers;
}

auto mailPart(Range)(Range range, string[string] headers)
@nogc {
	return MailPart!Range(range, headers);
}

// TODO: proper header folding. As a workaround we currently always fold whenever we find "; "
private auto foldHeaderValues(R)(scope R range) {
	alias Range = typeof(range.byChar);

	// Sanity check
	static assert(is(Unqual!(ElementType!Range) == char));

	static struct Result {
		uint m_state = 0;
		char m_charOut;
		Range m_range;

		this(Range range) {
			m_range = range;

			if (!m_range.empty) m_charOut = m_range.front;
		}

		@property bool empty() const @nogc { return m_range.empty; }

		void popFront()
		@nogc {
			if (m_state != 0) {
				const result = "\r\n\t";
				const index = m_state - 1;

				if (index < result.length) {
					m_charOut = result[index];

					m_state += 1;
				} else {
					while (!m_range.empty && m_range.front == ' ') {
						m_range.popFront();
					}

					if (!m_range.empty) {
						m_charOut = m_range.front;
					}

					m_state = 0;
				}
			} else {
				m_range.popFront();

				if (!m_range.empty) {
					m_charOut = m_range.front;

					if (m_charOut == ';') {
						m_state = 1;

						m_range.popFront();
					}
				}
			}
		}

		@property char front() const @nogc { return m_charOut; }
	}

	return Result(range.byChar).byDchar;
}

unittest {
	assert(foldHeaderValues(`text/html; charset="utf-8"`).text == "text/html;\r\n\tcharset=\"utf-8\"");
}

auto serializeHeaders(const string[string] headers)
@nogc {
	return headers.byKeyValue()
		.map!((pair) {
			return chain(pair.key, ": ", foldHeaderValues(pair.value), "\r\n");
		})
		.joiner
		.chain("\r\n")
		.byDchar;
}

unittest {
	auto headers = [
		"Foo": "bar",
		"Content-Type": `text/html; charset="utf-8"`,
	];
	assert(serializeHeaders(headers).text == "Foo: bar\r\nContent-Type: text/html;\r\n\tcharset=\"utf-8\"\r\n\r\n");
}

auto joinParts(R, Parts...)(const string boundary, MailPart!R part, Parts other_parts)
@nogc {
	static if (other_parts.length > 0) {
		const subparts = joinParts(boundary, other_parts);
	} else {
		const subparts = "";
	}

	return chain(
		"------=_", boundary, "\r\n",
		serializeHeaders(part.headers),
		part.content, "\r\n",
		subparts);
}

// TODO: Write tests for this.
auto mailMixed(Parts...)(Parts parts)
{
	const boundary = randomUUID().toString();
	auto headers = [
		"Content-Type": `multipart/mixed; boundary="----=_%s"`.format(boundary),
	];

	return mailPart(
		chain(
			joinParts(boundary, parts),
			"------=_", boundary, "--\r\n\r\n"),
		headers);
}

auto mailRelated(Parts...)(Parts parts)
{
	const boundary = randomUUID().toString();
	auto headers = [
		"Content-Type": `multipart/related; boundary="----=_%s"`.format(boundary),
	];

	return mailPart(
		chain(
			joinParts(boundary, parts),
			"------=_", boundary, "--\r\n\r\n"),
		headers);
}

auto mailAlternative(R1, R2)(MailPart!R1 part_a, MailPart!R2 part_b, string boundary = "")
{
	if (boundary.empty) {
		boundary = randomUUID().toString();
	}

	auto headers = [
		"Content-Type": `multipart/alternative; boundary="----=_%s"`.format(boundary),
	];

	auto c = chain(
		"------=_", boundary, "\r\n",

		serializeHeaders(part_a.headers),
		part_a.content, "\r\n",

		"------=_", boundary, "\r\n",

		serializeHeaders(part_b.headers),
		part_b.content, "\r\n",

		"------=_", boundary, "--\r\n",
	);

	return mailPart(c, headers);
}

auto mailHtml(Range)(Range html)
{
	auto headers = [
		"Content-Type": `text/html; charset="utf-8"`,
		"Content-Transfer-Encoding": "quoted-printable",
	];

	return mailPart(html.quotedPrintable(), headers);
}

auto mailText(Range)(Range text)
{
	auto headers = [
		"Content-Type": `text/plain; charset="utf-8"`,
		"Content-Transfer-Encoding": "quoted-printable",
	];

	return mailPart(text.quotedPrintable(), headers);
}

auto encodeData(Range)(Range data) {
	return Base64.encoder(data.chunks(57))
		.map!(chunk => chain(chunk, "\r\n"))
		.joiner
		.chain("\r\n")
		.byDchar;
}

auto mailAttachment(Range)(Range data, in string mime, in string name)
{
	auto headers = [
		"Content-Type": `%s; name="%s"`.format(mime, name),
		"Content-Transfer-Encoding": "base64",
		"Content-Disposition": `attachment; filename="%s"`.format(name),
	];

	return mailPart(encodeData(data), headers);
}

auto mailInlineImage(Range)(Range data, in string mime, in string content_id, in string name)
{
	auto headers = [
		"Content-Type": `%s; name="%s"`.format(mime, name),
		"Content-Transfer-Encoding": "base64",
		"Content-ID": `<%s>`.format(content_id),
	];

	return mailPart(encodeData(data), headers);
}

void setContent(Range)(Mail email, MailPart!Range part)
{
	foreach (key, value; part.headers) {
		email.headers[key] = value;
	}

	email.bodyText = part.content.text;
}

unittest {
	auto mail = mailText("blablabla");
	assert(mail.content.text == "blablabla");
	assert(mail.headers["Content-Transfer-Encoding"] == "quoted-printable");
	assert(mail.headers["Content-Type"] == `text/plain; charset="utf-8"`);
}

unittest {
	auto mail = mailText("This line is exactly 76 chars long, to make sure we put the '.' on newline-.");
	assert(mail.content.text == "This line is exactly 76 chars long, to make sure we put the '.' on newline-=\r\n..");
	assert(mail.headers["Content-Transfer-Encoding"] == "quoted-printable");
	assert(mail.headers["Content-Type"] == `text/plain; charset="utf-8"`);
}

unittest {
	auto mail = mailHtml(`<html><head></head><body><span class="hoopa">span content</span></body></html>`);
	assert(mail.content.text == "<html><head></head><body><span class=3D\"hoopa\">span content</span></body></=\r\nhtml>");
	assert(mail.headers["Content-Transfer-Encoding"] == "quoted-printable");
	assert(mail.headers["Content-Type"] == `text/html; charset="utf-8"`);
}

unittest {
	auto alt = mailAlternative(mailText("span content"), mailHtml(`<html><head></head><body><span class="hoopa">span content</span></body></html>`),"boundary");
	assert(alt.content.text ==
		"------=_boundary\r\nContent-Transfer-Encoding: quoted-printable\r\nContent-Type: text/plain;\r\n\tcharset=\"utf-8\"\r\n\r\nspan content\r\n------=_boundary\r\nContent-Transfer-Encoding: quoted-printable\r\nContent-Type: text/html;\r\n\tcharset=\"utf-8\"\r\n\r\n<html><head></head><body><span class=3D\"hoopa\">span content</span></body></=\r\nhtml>\r\n------=_boundary--\r\n");
	assert(alt.headers["Content-Type"] == "multipart/alternative; boundary=\"----=_boundary\"");
}

unittest {
	auto rel = mailRelated(mailAlternative(mailText("span content"), mailHtml("<html><head></head><body><span class=\"hoopa\">span content</span></body></html>"),"boundary"));
	//assert(rel.content.text ==
	//	"------=_boundary2\r\nContent-Type: multipart/alternative;\r\n\tboundary=\"----=_boundary\"\r\n\r\n------=_boundary\r\nContent-Transfer-Encoding: quoted-printable\r\nContent-Type: text/plain;\r\n\tcharset=\"utf-8\"\r\n\r\nspan content\r\n------=_boundary\r\nContent-Transfer-Encoding: quoted-printable\r\nContent-Type: text/html;\r\n\tcharset=\"utf-8\"\r\n\r\n<html><head></head><body><span class=3D\"hoopa\">span content</span></body></=\r\nhtml>\r\n------=_boundary--\r\n\r\n------=_boundary2--\r\n\r\n");
	//assert(rel.headers["Content-Type"] == "multipart/related; boundary=\"----=_boundary2\"");
}

unittest {
	auto m = mailAttachment(cast(immutable(ubyte[]))"span content\r\n", "text/file", "file.txt");
	assert(m.content.text == "c3BhbiBjb250ZW50DQo=\r\n\r\n");
	assert(m.headers["Content-Disposition"] == "attachment; filename=\"file.txt\"");
	assert(m.headers["Content-Transfer-Encoding"] == "base64");
	assert(m.headers["Content-Type"] == "text/file; name=\"file.txt\"");
}
